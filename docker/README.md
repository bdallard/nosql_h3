# 🐳 Workshop Docker & Python 🐳

## Introduction 
Docker est un logiciel libre permettant de lancer des applications dans des conteneurs logiciels.
Il ne s'agit pas de virtualisation, mais de conteneurisation, une forme plus légère qui s'appuie sur certaines parties de la machine hôte pour son fonctionnement. Cette approche permet d'accroître la flexibilité et la portabilité d’exécution d'une application, laquelle va pouvoir tourner de façon fiable et prévisible sur une grande variété de machines hôtes, que ce soit sur la machine locale, un cloud privé ou public, etc. Plus d'information sur [wikipedia](https://en.wikipedia.org/wiki/Docker_(software)) et la documentation officielle [ici](https://docs.docker.com/v17.09/). 

Docker a été inventé pour résoudre un probleme classique que nous avons tous rencontré en informatique. 
Un meme parle toujours mieux que milles mots : 
<div style="text-align:center">
<img src="https://external-preview.redd.it/aR6WdUcsrEgld5xUlglgKX_0sC_NlryCPTXIHk5qdu8.jpg?auto=webp&s=5fe64dd318eec71711d87805d43def2765dd83cd">
</div>


## Vos missions

### Jour 1 - matin : 

* comprendre la différence entre images et containers et à quoi sert un `Dockerfile` 
* installer docker sur votre machine (Ubuntu de préférence) 
* lancer votre premier container ubuntu, l'équivalent du *hello-world* de docker  
* regarder si votre container est bien lancer 
* faire un résumé type `cheat sheet` des principales commandes dockers relatives aux images et containers 
* vérifier en vous connectant à votre container qu'il est bien `up` et qu'il s'aggit bien 
* créer un repo github pour les deux jours  ⚠️avec un README.md explicite svp⚠️
* faire un push de votre travail sur votre repo et m'envoyer le lien par mail **avec pour objet S1-1-NOM-PRENOM**


### Jour 1 - après midi : 

* coder un container type *data engineering sandbox* à partir d'une image Ubuntu    contenant : 
	* un espace de travail appelé workspace 
	* python3, pip3 et vim 
	* une installation automatique du fichier `requirements.txt`
	* un set up des credentials git pour l'utilisateur du container
	* une exposition du port 8000 
	* pour finir le container devra lancer jupyter notebook avec votre git clonné à la racine  
* faire un push de votre travail sur votre repo et m'envoyer le lien par mail **avec pour objet S1-1-NOM-PRENOM**


### Jour 2 - matin : 

* faire un résumé type *cheat sheet* des principales commandes des `Dockerfile`
* se renseigner sur les API de façon générale et à quoi cela sert, quel interet peut avoir une entreprise à développer des services API
* regarder le quickstart de flask et compléter le fichier `simple_api.py`, expliquer ce qu'est une route 
* charger le ficher `books.json` dans votre app.py et créer une route `/books` qui renvoie le fichier 
* ajouter une route qui renvoie un livre 
* faire un push de votre travail sur votre repo et m'envoyer le lien par mail **avec pour objet S2-1-NOM-PRENOM**


### Jour 2 - après midi : 

* écrire un `Dockerfile` pour le fichier `simple_api.py`
* builder votre image et lancer un container   
* verifier que votre container est bien actif avec les commandes docker 
* faire un fichier `test_api.sh` pour tester votre api
* faire un push de votre travail sur votre repo et m'envoyer le lien par mail **avec pour objet S2-2-NOM-PRENOM**
 


### Les livrables 
Vous avez des consignes assez explicites pour ces deux jours. J'attends comme énoncé ci-dessus **a minima deux push sur git par jours**. 
Si vous voulez en faire plus cela me permettra de suivre votre avancement. Force à vous durant cette période compliquée. 
 

#### Une commande qui peut vous servir   
```bash
$ sudo groupadd docker
$ sudo gpasswd -a $USER docker
```

## Ressource
- Doc officiel : https://docs.docker.com/v17.09/
- Dockerfile : https://linuxize.com/post/how-to-build-docker-images-with-dockerfile/
- Pour les plus avancées : https://www.katacoda.com/courses/container-runtimes