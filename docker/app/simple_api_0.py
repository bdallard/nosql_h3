"""
	Example of mini API in Flask
"""


from flask import Flask

app = Flask(__name__)

@app.route('/')
def home():
	return "This is Flask on your localhost  (: "


if __name__ == '__main__':
	app.run()

